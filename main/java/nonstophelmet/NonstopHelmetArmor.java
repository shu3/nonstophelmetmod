package nonstophelmet;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class NonstopHelmetArmor extends ItemArmor {
	ArrayList<HashMap> server_pos_history;
	private static int LIMIT_SEC = 3;

	public NonstopHelmetArmor(ArmorMaterial material, int id, int placement) {
		super(material, id, placement);
		
		if (placement == 0) {
			this.setTextureName(NonstopHelmetMod.MODID + ":nonstop_helmet");
		}
		server_pos_history = new ArrayList();
		addNowPos(server_pos_history, 0.0d, 0.0d, 0.0d);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type){
		if (stack.getItem() == NonstopHelmetMod.helmet) {
			return NonstopHelmetMod.MODID + ":textures/models/armor/helmet_layer.png";
		}
		return null;
	}
	
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack){
		if(itemStack.getItem() == NonstopHelmetMod.helmet){
			if (world.isRemote == true) {
				return;
			}
			
			ArrayList<HashMap> pos_history = null;
			pos_history = server_pos_history;
			if (player.isDead || player.isPlayerSleeping()) {
				return;
			}

			Date now = new Date();
			int before = pos_history.size();

			if (isStillSamePos(pos_history, player) && !isAfterSec(pos_history, 20)) {
				if (isAfterSec(pos_history, 1)) {
					//	System.out.println("AfterSec(1): before=" + before);
					// 1秒以上経過、同じポジション
					addNowPos(pos_history, player.posX, player.posY, player.posZ);
				}
			}
			else {
				// System.out.println("Difference Pos");
				// 同じポジションでなければクリア
				pos_history.clear();
				addNowPos(pos_history, player.posX, player.posY, player.posZ);
			}

			int after = pos_history.size();
			if ((after - before) == 1) {
				// 3秒経過後
				if ((after-2) == LIMIT_SEC) {
					world.playSoundEffect(
							(double)player.posX + 0.5D,
							(double)player.posY + 0.5D,
							(double)player.posZ + 0.5D,
							NonstopHelmetMod.MODID + ":bubu", 3.0F, 12.0f);
					player.setHealth(0.0f);
					player.setDead();
					pos_history.clear();
					addNowPos(pos_history, player.posX, player.posY, player.posZ);
				}
				else {
					world.playSoundEffect(
							(double)player.posX + 0.5D,
							(double)player.posY + 0.5D,
							(double)player.posZ + 0.5D,
							NonstopHelmetMod.MODID + ":pi", 3.0F, 12.0f);
				}
			}
		}
	}
	
	private boolean isStillSamePos(ArrayList<HashMap> pos_history, EntityPlayer player) {
		HashMap last = pos_history.get(pos_history.size()-1);
		if (BigDecimal.valueOf(player.posX).equals(last.get("x")) &&
			BigDecimal.valueOf(player.posY).equals(last.get("y")) &&
			BigDecimal.valueOf(player.posZ).equals(last.get("z"))) {
			return true;
		}
		return false;
	}
	
	private boolean isAfterSec(ArrayList<HashMap> pos_history, int sec) {
		Date now = new Date();
		HashMap last = pos_history.get(pos_history.size()-1);
		long diff = now.getTime() - ((Date)last.get("at")).getTime();
		if (diff > (sec * 1000)) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	private void addNowPos(ArrayList pos_history, double x, double y, double z) {
		Date now = new Date();
		HashMap hash = new HashMap();
		hash.put("at", now);
		hash.put("x", BigDecimal.valueOf(x));
		hash.put("y", BigDecimal.valueOf(y));
		hash.put("z", BigDecimal.valueOf(z));
		pos_history.add(hash);
	}
	
	private boolean isInBed(World world, EntityPlayer player) {
		int x = player.playerLocation.posX;
		int y = player.playerLocation.posY;
		int z = player.playerLocation.posZ;
		return (world.getBlock(x, y, z).isBed(world, x, y, z, player) ||
				world.getBlock(x, y-1, z).isBed(world, x, y-1, z, player));
	}
}
