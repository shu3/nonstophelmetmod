package nonstophelmet;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = NonstopHelmetMod.MODID, version = NonstopHelmetMod.VERSION)
public class NonstopHelmetMod {
    public static final String MODID = "nonstophelmet";
    public static final String VERSION = "1.0";
    public static int helmetId;
    public static Item helmet;

    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
    	helmet = new NonstopHelmetArmor(ItemArmor.ArmorMaterial.IRON, helmetId, 0)
			.setUnlocalizedName("nonstop_helmet");
    	GameRegistry.registerItem(helmet, "nonstop_helmet");
    	GameRegistry.addShapelessRecipe(new ItemStack(this.helmet, 1), Block.blockRegistry.getObject("dirt"));
	}
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// some example code
    }
}
